PCSX2 1.7.0-20210131221406 
Savestate version: 0x9a1a0000

Host Machine Init:
	Operating System =  Linux 5.10.12-artix1-1 x86_64
	Physical RAM     =  5930 MB
	CPU name         =  AMD Ryzen 3 3200G with Radeon Vega Graphics
	Vendor/Model     =  AuthenticAMD (stepping 01)
	CPU speed        =  3.597 ghz (4 logical threads)
	x86PType         =  Standard OEM
	x86Flags         =  178bfbff 7ed8320b
	x86EFlags        =  2fd3fbff

x86 Features Detected:
	SSE2.. SSE3.. SSSE3.. SSE4.1.. SSE4.2.. AVX.. AVX2.. FMA
	SSE4a 
	Pcsx2 was compiled as 64-bits.

Installing POSIX SIGSEGV handler...
Reserving memory for recompilers...

Loading plugins from /usr/lib/PCSX2...
	Bound   GS: libGSdx-AVX2.so [GSdx 64-bit (GCC 10.2.0 AVX/AVX) 1.2.0]
Plugins loaded successfully.

[GameDB] 10326 games on record (loaded in 250ms)
HLE Notice: ELF does not have a path.

Initializing plugins...
	Init GS
Plugins initialized successfully.

Patches: No CRC found, using 00000000 instead.
isoFile open ok: /home/doonoo/Games/Roms/PS2/WWE SmackDown! Here Comes the Pain (USA).iso
	Image type  = DVD
 * CDVD Disk Open: DVD, Single layer or unknown:
 * * Track 1: Data (Mode 1) (2209584 sectors)
Opening plugins...
	Opening GS
EGL: Supported extensions: EGL_EXT_device_base EGL_EXT_device_enumeration EGL_EXT_device_query EGL_EXT_platform_base EGL_KHR_client_get_all_proc_addresses EGL_EXT_client_extensions EGL_KHR_debug EGL_EXT_platform_device EGL_EXT_platform_wayland EGL_KHR_platform_wayland EGL_EXT_platform_x11 EGL_KHR_platform_x11 EGL_MESA_platform_gbm EGL_KHR_platform_gbm EGL_MESA_platform_surfaceless
EGL: select X11 platform
ATTENTION: default value of option mesa_glthread overridden by environment.
ATTENTION: option value of option mesa_glthread ignored.
OpenGL information. GPU: AMD Radeon(TM) Vega 8 Graphics (RAVEN, DRM 3.40.0, 5.10.12-artix1-1, LLVM 11.0.1). Vendor: AMD. Driver: (Core Profile) Mesa 20.3.4
INFO: GL_ARB_sparse_texture is NOT SUPPORTED
INFO: GL_ARB_sparse_texture2 is NOT SUPPORTED
INFO: GL_ARB_gpu_shader5 is available
INFO: GL_ARB_shader_image_load_store is available
INFO: GL_ARB_compute_shader is available
INFO: GL_ARB_shader_storage_buffer_object is available
INFO: GL_ARB_texture_view is available
INFO: GL_ARB_vertex_attrib_binding is available
INFO: GL_ARB_clear_texture is available
INFO: GL_ARB_multi_bind is available
INFO: GL_ARB_direct_state_access is available
INFO: GL_ARB_texture_barrier is available
INFO: GL_ARB_get_texture_sub_image is available

Current Renderer: OpenGL
Available VRAM/RAM:3840MB for textures
GSdx Lookup CRC:00000000
McdSlot 0 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd001.ps2
GSdx Lookup CRC:00000000
McdSlot 1 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd002.ps2
Plugins opened successfully.
Failed to GetNetAdapter()
48000 SampleRate: 
Request SDL audio driver: pulseaudio
Opened SDL audio driver: pulseaudio
PAD: controller (Wireless Controller) detected with rumble support, GUID:050000004c050000cc09000000810000
EE/iR5900-32 Recompiler Reset
	Bios Found: USA     v01.60(07/02/2002)  Console
	BIOS r module not found, skipping...
	BIOS r module not found, skipping...
	BIOS e module not found, skipping...
# Initialize memory (rev:3.63, ctm:196Mhz, cpuclk:147Mhz detected)
Frame buffer size set to  512x512 (1536x1536)

PlayStation 2 ======== Hard reset boot
 ROMGEN=2002-0207, IOP info (CPUID=1f, CACH_CONFIG=0, 2MB, IOP mode)
 <20020207-164243,ROMconf,PS20160AC20020207.bin:11552>
# Total accessable memory size: 32 MB (B:2:8:0) (363:2:7c30)
# TLB spad=0 kernel=1:12 default=13:30 extended=31:38
# Initialize Start.
# Initialize GS ...
# Initialize INTC ...
# Initialize TIMER ...
# Initialize DMAC ...
# Initialize VU1 ...
# Initialize VIF1 ...
# Initialize GIF ...
# Initialize VU0 ...
# Initialize VIF0 ...
# Initialize IPU ...
# Initialize FPU ...
# Initialize User Memory ...
PAD: controller (Wireless Controller) detected with rumble support, GUID:050000004c050000cc09000000810000
# Initialize Scratch Pad ...
# Initialize Done.

EE DECI2 Manager version 0.06 Feb  7 2002 16:41:20
  CPUID=2e20, BoardID=0, ROMGEN=2002-0207, 32M

Patches: No CRC found, using 00000000 instead.
(SYSTEM.CNF) Detected PS2 Disc = cdrom0:\SLUS_207.87;1
(SYSTEM.CNF) Software version = 1.00
(SYSTEM.CNF) Disc region type = NTSC
found 24475 symbols
ELF (cdrom0:\SLUS_207.87;1) Game CRC = 0x9B85B093, EntryPoint = 0x00100008
(SYSTEM.CNF) Detected PS2 Disc = cdrom0:\SLUS_207.87;1
(SYSTEM.CNF) Software version = 1.00
(SYSTEM.CNF) Disc region type = NTSC

IOP Realtime Kernel Ver.0.9.1
    Copyright 1999 (C) Sony Computer Entertainment Inc. 
Reboot service module.(99/11/10)
cdvd driver module version 0.1.1 (C)SCEI
Load File service.(99/11/05)
Multi Threaded Fileio module.(99/11/15) 
iop heap service (99/11/03)
loadelf: fname cdrom0:¥SLUS_207.87;1 secname all
loadelf version 3.30
Input ELF format filename = cdrom0:¥SLUS_207.87;1
0 00100000 00389880 .........................................................
Loaded, cdrom0:¥SLUS_207.87;1
start address 0x100008
gp address 00000000
# Restart Without Memory Clear.
# Initialize GS ...
# Initialize INTC ...
# Initialize TIMER ...
# Initialize DMAC ...
# Initialize VU1 ...
# Initialize VIF1 ...
# Initialize GIF ...
# Initialize VU0 ...
# Initialize VIF0 ...
# Initialize IPU ...
# Initialize FPU ...
# Initialize Scratch Pad ...
# Restart Without Memory Clear Done.
Elf entry point @ 0x00100008 about to get recompiled. Load patches first.
[GameDB] Searching for 'slus-20787' in GameDB
[GameDB] Found 'slus-20787' in GameDB
[GameDB] Searching for patch with CRC '9b85b093'
[GameDB] No CRC-specific patch or default patch found
Overall 0 Widescreen hacks loaded
Loading patch '9B85B093.pnach' from archive '/usr/share/PCSX2/cheats_ws.zip'
comment: Widescreen hack by ElHecht
(Wide Screen Cheats DB) Patches Loaded: 4
[GameDB] Searching for 'slus-20787' in GameDB
[GameDB] Found 'slus-20787' in GameDB
[GameDB] Searching for patch with CRC '9b85b093'
[GameDB] No CRC-specific patch or default patch found
GSdx Lookup CRC:9B85B093
GSdx Lookup CRC:9B85B093
Welcome to CodeWarrior for PS2
Disk Type is DVD

Get Reboot Request From EE
ROM directory not found

PlayStation 2 ======== Update rebooting..

PlayStation 2 ======== Update reboot complete
cdvdman Init

IOP Realtime Kernel Ver. 2.2
    Copyright 1999-2002 (C) Sony Computer Entertainment Inc. 
Reboot service module.(99/11/10)
cdvd driver module version 0.1.1 (C)SCEI
Load File service.(99/11/05)
Multi Threaded Fileio module.(99/11/15) 
iop heap service (99/11/03)
Disk Type is DVD
loadbuffer: addrres 45d00 args 0 arg 
loadbuffer: id 25, ret 0
loadbuffer: addrres 48900 args 0 arg 
loadbuffer: id 26, ret 0
loadbuffer: addrres 4d600 args 0 arg 
loadbuffer: id 27, ret 0
loadbuffer: addrres 61d00 args 0 arg 
loadbuffer: id 28, ret 2
loadbuffer: addrres 48900 args 0 arg 
loadbuffer: id 29, ret 2
loadbuffer: addrres 50e00 args 0 arg 
loadbuffer: id 30, ret 0
loadbuffer: addrres 48900 args 0 arg 
SDR driver version 4.0.1 (C) SCEI
 Exit rsd_main 
loadbuffer: id 31, ret 0
loadbuffer: addrres 67700 args 0 arg 
loadbuffer: id 32, ret 0
loadbuffer: addrres 52900 args 0 arg 
loadbuffer: id 33, ret 0
loadbuffer: addrres 46600 args 0 arg 
loadbuffer: id 34, ret 0
loadbuffer: addrres 48900 args 0 arg 
loadbuffer: id 35, ret 0
loadbuffer: addrres 54600 args 0 arg 
loadbuffer: id 36, ret 0
loadbuffer: addrres 54600 args 24 arg maxtrack=16
loadbuffer: id 37, ret 0
loadbuffer: addrres 54600 args 24 arg maxtrack=16
loadbuffer: id 38, ret 0
loadbuffer: addrres bfa00 args 22 arg maxtrack=3
loadbuffer: id 39, ret 0
loadbuffer: addrres 76e00 args 22 arg maxtrack=8
loadbuffer: id 40, ret 0
loadbuffer: addrres c0e00 args 0 arg maxtrack=8
loadbuffer: id 41, ret 0
loadbuffer: addrres c8800 args 0 arg maxtrack=8
loadbuffer: id 42, ret 0
loadbuffer: addrres cf000 args 0 arg maxtrack=8



@@@@ CMPILE DATE Sep 13 2003 TIME 23:39:03 @@@@ 
IOP memory  Free 0x100f00(max:0x0f9a00)/0x200000
loadbuffer: id 43, ret 0
loadbuffer: addrres eda00 args 105 arg sdinit=0
CRI ADX Driver Ver.8.94(Sep 14 2003 00:12:01)
CRI ADX Driver : sceSdInit Call = 0
CRI ADX Driver : Main Thread Priority = 47
CRI ADX Driver : PSM Thread Priority  = 47
CRI ADX Driver : DTX Thread Priority  = 48
CRI ADX Driver : SPU CORE Number = 0
loadbuffer: id 44, ret 0
(UpdateVSyncRate) Mode Changed to NTSC.
(UpdateVSyncRate) FPS Limit Changed : 59.94 fps
Frame buffer size set to  640x512 (1920x1536)
libpad: pad port is already open [0][0]
libpad: pad port is already open [0][1]
libpad: pad port is already open [0][2]
libpad: pad port is already open [0][3]
libpad: pad port is already open [1][0]
libpad: pad port is already open [1][1]
libpad: pad port is already open [1][2]
libpad: pad port is already open [1][3]
DVCI: "BGM¥BGM000.AFS" found.
DVCI: "BGM¥BGM001.AFS" found.
DVCI: "BGM¥BGM002.AFS" found.
DVCI: "BGM¥BGM003.AFS" found.
DVCI: "BGM¥BGM004.AFS" found.
DVCI: "BGM¥BGM999.AFS" Not found.
DVCI: "BGM¥CHEER.AFS" found.
DVCI: "BGM¥BG_ENV.AFS" found.
DVCI: "MOVIE¥NMV000.AFS" found.
DVCI: "MOVIE¥NMV001.AFS" found.
DVCI: "MOVIE¥NMV002.AFS" found.
DVCI: "MOVIE¥NMV003.AFS" found.
DVCI: "MOVIE¥NMV999.AFS" Not found.
DVCI: "MOVIE¥LOGOS.AFS" found.
DVCI: "MOVIE¥LOGOS_S.AFS" Not found.
DVCI: "ED0¥ED02.PAC" found.
DVCI: "ED0¥ED03.PAC" found.
DVCI: "ED0¥ED04.PAC" found.
DVCI: "ED0¥ED05.PAC" found.
DVCI: "ED0¥ED06.PAC" found.
DVCI: "ED0¥ED07.PAC" found.
DVCI: "ED0¥ED08.PAC" found.
DVCI: "ED0¥ED09.PAC" found.
DVCI: "ED1¥ED10.PAC" found.
DVCI: "ED1¥ED11.PAC" found.
DVCI: "ED1¥ED12.PAC" found.
DVCI: "ED1¥ED13.PAC" found.
DVCI: "ED1¥ED14.PAC" found.
DVCI: "ED1¥ED15.PAC" found.
DVCI: "ED1¥ED16.PAC" found.
DVCI: "ED1¥ED17.PAC" found.
DVCI: "ED1¥ED18.PAC" found.
DVCI: "ED1¥ED19.PAC" found.
DVCI: "ED2¥ED20.PAC" found.
DVCI: "ED2¥ED21.PAC" found.
DVCI: "ED2¥ED22.PAC" found.
DVCI: "ED2¥ED23.PAC" found.
DVCI: "ED2¥ED24.PAC" found.
DVCI: "ED2¥ED25.PAC" found.
DVCI: "ED2¥ED26.PAC" found.
DVCI: "ED2¥ED27.PAC" found.
DVCI: "ED2¥ED28.PAC" found.
DVCI: "ED2¥ED29.PAC" found.
DVCI: "ED3¥ED30.PAC" found.
DVCI: "ED3¥ED31.PAC" found.
DVCI: "ED3¥ED32.PAC" found.
DVCI: "ED3¥ED33.PAC" found.
DVCI: "ED3¥ED34.PAC" found.
DVCI: "ED3¥ED35.PAC" found.
DVCI: "ED3¥ED36.PAC" found.
DVCI: "ED3¥ED37.PAC" found.
DVCI: "ED3¥ED38.PAC" found.
DVCI: "ED3¥ED39.PAC" found.
DVCI: "ED4¥ED40.PAC" found.
DVCI: "ED4¥ED41.PAC" found.
DVCI: "ED4¥ED42.PAC" found.
DVCI: "ED4¥ED43.PAC" found.
DVCI: "ED4¥ED44.PAC" found.
DVCI: "ED4¥ED45.PAC" found.
DVCI: "ED4¥ED46.PAC" found.
DVCI: "ED4¥ED47.PAC" found.
DVCI: "ED4¥ED48.PAC" found.
DVCI: "ED4¥ED49.PAC" found.
DVCI: "ED5¥ED50.PAC" found.
DVCI: "ED5¥ED51.PAC" found.
DVCI: "ED5¥ED52.PAC" found.
DVCI: "ED5¥ED53.PAC" found.
DVCI: "ED5¥ED54.PAC" found.
DVCI: "ED5¥ED55.PAC" found.
DVCI: "ED5¥ED56.PAC" found.
DVCI: "PAC¥BG.PAC" found.
DVCI: "PAC¥CA.PAC" found.
DVCI: "PAC¥CH.PAC" found.
DVCI: "PAC¥CH2.PAC" found.
DVCI: "PAC¥EDIT.PAC" found.
DVCI: "PAC¥ENTR2D.PAC" found.
DVCI: "PAC¥EVT.PAC" found.
DVCI: "PAC¥EVTDORA.PAC" found.
DVCI: "PAC¥EVTENT.PAC" found.
DVCI: "PAC¥EVTTKME.PAC" found.
DVCI: "PAC¥GAME2D.PAC" found.
DVCI: "PAC¥GM.PAC" found.
DVCI: "PAC¥HALO.PAC" found.
DVCI: "PAC¥M.PAC" found.
DVCI: "PAC¥MENU.PAC" found.
DVCI: "PAC¥MISC.PAC" found.
DVCI: "PAC¥MOVES.PAC" found.
DVCI: "PAC¥MOVIE.PAC" found.
DVCI: "PAC¥SOUND.PAC" found.
DVCI: "PAC¥STABLE.PAC" found.
DVCI: Total 87 files.
DVCI: Warning, Can't load all cache file.
PS2RNA: sceSifAllocIopHeap(266304) ret=0x0010de00
PS2RNA: sceSifAllocIopHeap(2256) ret=0x000c2f00
PS2RNA: sceSifAllocIopHeap(16448) ret=0x000cf000
SDR callback thread created
se load bank[1] 
se load & trans end bank[1] 
se load bank[1] 
se load bank[2] 
se load bank[3] 
se load bank[6] 
se load bank[8] 
se load & trans end bank[1] 
se load & trans end bank[2] 
se load & trans end bank[3] 
se load & trans end bank[6] 
se load & trans end bank[8] 
padman: *** VBLANK OVERLAP ***
padman: *** VBLANK OVERLAP ***
se load bank[1] 
se load bank[2] 
se load bank[6] 
se load bank[8] 
se load & trans end bank[1] 
se load & trans end bank[2] 
se load & trans end bank[6] 
se load & trans end bank[8] 
se load bank[1] 
se load & trans end bank[1] 
Closing plugins...
	Closing GS
Delete 0 Shaders, 80 Programs, 28 Pipelines
Plugins closed successfully.
[GameDB] Searching for 'slus-20787' in GameDB
[GameDB] Found 'slus-20787' in GameDB
[GameDB] Searching for patch with CRC '9b85b093'
[GameDB] No CRC-specific patch or default patch found
isoFile open ok: /home/doonoo/Games/Roms/PS2/WWE SmackDown! Here Comes the Pain (USA).iso
	Image type  = DVD
 * CDVD Disk Open: DVD, Single layer or unknown:
 * * Track 1: Data (Mode 1) (2209584 sectors)
Opening plugins...
	Opening GS
EGL: Supported extensions: EGL_EXT_device_base EGL_EXT_device_enumeration EGL_EXT_device_query EGL_EXT_platform_base EGL_KHR_client_get_all_proc_addresses EGL_EXT_client_extensions EGL_KHR_debug EGL_EXT_platform_device EGL_EXT_platform_wayland EGL_KHR_platform_wayland EGL_EXT_platform_x11 EGL_KHR_platform_x11 EGL_MESA_platform_gbm EGL_KHR_platform_gbm EGL_MESA_platform_surfaceless
EGL: select X11 platform
ATTENTION: default value of option mesa_glthread overridden by environment.
ATTENTION: option value of option mesa_glthread ignored.
mesa: for the --simplifycfg-sink-common option: may only occur zero or one times!
mesa: for the --global-isel-abort option: may only occur zero or one times!
mesa: for the --amdgpu-atomic-optimizations option: may only occur zero or one times!
mesa: for the --structurizecfg-skip-uniform-regions option: may only occur zero or one times!
Current Renderer: OpenGL
Available VRAM/RAM:3840MB for textures
GSdx Lookup CRC:9B85B093
GSdx Lookup CRC:9B85B093
McdSlot 0 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd001.ps2
McdSlot 1 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd002.ps2
Plugins opened successfully.
Failed to GetNetAdapter()
48000 SampleRate: 
Request SDL audio driver: pulseaudio
Opened SDL audio driver: pulseaudio
PAD: controller (Wireless Controller) detected with rumble support, GUID:050000004c050000cc09000000810000
Closing plugins...
	Closing GS
Delete 0 Shaders, 45 Programs, 28 Pipelines
Plugins closed successfully.
Decommitting host memory for virtual systems...
	HotSwapping to new ISO src image!
HLE Notice: ELF does not have a path.

[GameDB] Searching for 'slus-20787' in GameDB
[GameDB] Found 'slus-20787' in GameDB
[GameDB] Searching for patch with CRC '9b85b093'
[GameDB] No CRC-specific patch or default patch found
isoFile open ok: /home/doonoo/Games/Roms/PS2/Yakuza (USA).iso
	Image type  = DVD
 * CDVD Disk Open: DVD, Single layer or unknown:
 * * Track 1: Data (Mode 1) (2144000 sectors)
Opening plugins...
	Opening GS
EGL: Supported extensions: EGL_EXT_device_base EGL_EXT_device_enumeration EGL_EXT_device_query EGL_EXT_platform_base EGL_KHR_client_get_all_proc_addresses EGL_EXT_client_extensions EGL_KHR_debug EGL_EXT_platform_device EGL_EXT_platform_wayland EGL_KHR_platform_wayland EGL_EXT_platform_x11 EGL_KHR_platform_x11 EGL_MESA_platform_gbm EGL_KHR_platform_gbm EGL_MESA_platform_surfaceless
EGL: select X11 platform
ATTENTION: default value of option mesa_glthread overridden by environment.
ATTENTION: option value of option mesa_glthread ignored.
mesa: for the --simplifycfg-sink-common option: may only occur zero or one times!
mesa: for the --global-isel-abort option: may only occur zero or one times!
mesa: for the --amdgpu-atomic-optimizations option: may only occur zero or one times!
mesa: for the --structurizecfg-skip-uniform-regions option: may only occur zero or one times!
Current Renderer: OpenGL
Available VRAM/RAM:3840MB for textures
GSdx Lookup CRC:9B85B093
GSdx Lookup CRC:9B85B093
McdSlot 0 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd001.ps2
McdSlot 1 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd002.ps2
Plugins opened successfully.
Failed to GetNetAdapter()
48000 SampleRate: 
Request SDL audio driver: pulseaudio
PAD: controller (Wireless Controller) detected with rumble support, GUID:050000004c050000cc09000000810000
Opened SDL audio driver: pulseaudio
	Bios Found: USA     v01.60(07/02/2002)  Console
	BIOS r module not found, skipping...
	BIOS r module not found, skipping...
	BIOS e module not found, skipping...
EE/iR5900-32 Recompiler Reset
# Initialize memory (rev:3.63, ctm:393Mhz, cpuclk:295Mhz detected)

PlayStation 2 ======== Hard reset boot
 ROMGEN=2002-0207, IOP info (CPUID=1f, CACH_CONFIG=0, 2MB, IOP mode)
 <20020207-164243,ROMconf,PS20160AC20020207.bin:11552>
# Total accessable memory size: 32 MB (B:2:8:0) (363:2:7c30)
# TLB spad=0 kernel=1:12 default=13:30 extended=31:38
# Initialize Start.
# Initialize GS ...
# Initialize INTC ...
# Initialize TIMER ...
# Initialize DMAC ...
# Initialize VU1 ...
# Initialize VIF1 ...
# Initialize GIF ...
# Initialize VU0 ...
# Initialize VIF0 ...
# Initialize IPU ...
# Initialize FPU ...
# Initialize User Memory ...
# Initialize Scratch Pad ...
# Initialize Done.

EE DECI2 Manager version 0.06 Feb  7 2002 16:41:20
  CPUID=2e20, BoardID=0, ROMGEN=2002-0207, 32M

Patches: No CRC found, using 00000000 instead.
(SYSTEM.CNF) Detected PS2 Disc = cdrom0:\SLUS_213.48;1
(SYSTEM.CNF) Software version = 1.00
(SYSTEM.CNF) Disc region type = NTSC
found 0 symbols
ELF (cdrom0:\SLUS_213.48;1) Game CRC = 0x388F687B, EntryPoint = 0x00100008
(SYSTEM.CNF) Detected PS2 Disc = cdrom0:\SLUS_213.48;1
(SYSTEM.CNF) Software version = 1.00
(SYSTEM.CNF) Disc region type = NTSC

IOP Realtime Kernel Ver.0.9.1
    Copyright 1999 (C) Sony Computer Entertainment Inc. 
Reboot service module.(99/11/10)
cdvd driver module version 0.1.1 (C)SCEI
Load File service.(99/11/05)
Multi Threaded Fileio module.(99/11/15) 
iop heap service (99/11/03)
loadelf: fname cdrom0:¥SLUS_213.48;1 secname all
loadelf version 3.30
Input ELF format filename = cdrom0:¥SLUS_213.48;1
0 00100000 00056300 ......
Loaded, cdrom0:¥SLUS_213.48;1
start address 0x100008
gp address 00000000
# Restart Without Memory Clear.
# Initialize GS ...
# Initialize INTC ...
# Initialize TIMER ...
# Initialize DMAC ...
# Initialize VU1 ...
# Initialize VIF1 ...
# Initialize GIF ...
# Initialize VU0 ...
# Initialize VIF0 ...
# Initialize IPU ...
# Initialize FPU ...
# Initialize Scratch Pad ...
# Restart Without Memory Clear Done.
Elf entry point @ 0x00100008 about to get recompiled. Load patches first.
[GameDB] Searching for 'slus-21348' in GameDB
[GameDB] Found 'slus-21348' in GameDB
[GameDB] Searching for patch with CRC '388f687b'
[GameDB] No CRC-specific patch or default patch found
(GameDB) Enabled Gamefix: EETimingHack
Overall 0 Widescreen hacks loaded
(Wide Screen Cheats DB) Patches Loaded: 0
[GameDB] Searching for 'slus-21348' in GameDB
[GameDB] Found 'slus-21348' in GameDB
[GameDB] Searching for patch with CRC '388f687b'
[GameDB] No CRC-specific patch or default patch found
EE/iR5900-32 Recompiler Reset
Get Reboot Request From EE
GSdx Lookup CRC:388F687B
GSdx Lookup CRC:388F687B
ROM directory not found

PlayStation 2 ======== Update rebooting..

PlayStation 2 ======== Update reboot complete
cdvdman Init

IOP Realtime Kernel Ver. 2.2
    Copyright 1999-2002 (C) Sony Computer Entertainment Inc. 
Reboot service module.(99/11/10)
cdvd driver module version 0.1.1 (C)SCEI
Load File service.(99/11/05)
Multi Threaded Fileio module.(99/11/15) 
iop heap service (99/11/03)
end           = 006ECB80
SDR driver version 4.0.1 (C) SCEI
 Exit rsd_main 
CRI ADX Driver Ver.9.44sega(Sep 18 2005 19:59:06)
CRI ADX Driver : Main Thread Priority = 39
CRI ADX Driver : PSM Thread Priority  = 39
CRI ADX Driver : DTX Thread Priority  = 48
CRI ADX Driver : SPU CORE Number = 2
CRI ADX Driver : sceSdInit(SD_INIT_COLD) call.
SNDF Driver Ver 2.26(Aug  2 2005 18:41:26)
Unknown device 'hdd'
Known devices are  cdrom:(CD-ROM )  rom:(ROM/Flash)  tty:(CONSOLE) 
Unknown device 'hdd'
SRD: 64bit Host filesystem.
Closing plugins...
	Closing GS
Delete 0 Shaders, 42 Programs, 28 Pipelines
Plugins closed successfully.
[GameDB] Searching for 'slus-21348' in GameDB
[GameDB] Found 'slus-21348' in GameDB
[GameDB] Searching for patch with CRC '388f687b'
[GameDB] No CRC-specific patch or default patch found
isoFile open ok: /home/doonoo/Games/Roms/PS2/Yakuza (USA).iso
	Image type  = DVD
 * CDVD Disk Open: DVD, Single layer or unknown:
 * * Track 1: Data (Mode 1) (2144000 sectors)
Opening plugins...
	Opening GS
EGL: Supported extensions: EGL_EXT_device_base EGL_EXT_device_enumeration EGL_EXT_device_query EGL_EXT_platform_base EGL_KHR_client_get_all_proc_addresses EGL_EXT_client_extensions EGL_KHR_debug EGL_EXT_platform_device EGL_EXT_platform_wayland EGL_KHR_platform_wayland EGL_EXT_platform_x11 EGL_KHR_platform_x11 EGL_MESA_platform_gbm EGL_KHR_platform_gbm EGL_MESA_platform_surfaceless
EGL: select X11 platform
ATTENTION: default value of option mesa_glthread overridden by environment.
ATTENTION: option value of option mesa_glthread ignored.
mesa: for the --simplifycfg-sink-common option: may only occur zero or one times!
mesa: for the --global-isel-abort option: may only occur zero or one times!
mesa: for the --amdgpu-atomic-optimizations option: may only occur zero or one times!
mesa: for the --structurizecfg-skip-uniform-regions option: may only occur zero or one times!
Current Renderer: OpenGL
Available VRAM/RAM:3840MB for textures
GSdx Lookup CRC:388F687B
GSdx Lookup CRC:388F687B
McdSlot 0 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd001.ps2
McdSlot 1 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd002.ps2
Plugins opened successfully.
Failed to GetNetAdapter()
48000 SampleRate: 
Request SDL audio driver: pulseaudio
Opened SDL audio driver: pulseaudio
PAD: controller (Wireless Controller) detected with rumble support, GUID:050000004c050000cc09000000810000
Closing plugins...
	Closing GS
Delete 0 Shaders, 69 Programs, 28 Pipelines
Plugins closed successfully.
[GameDB] Searching for 'slus-21348' in GameDB
[GameDB] Found 'slus-21348' in GameDB
[GameDB] Searching for patch with CRC '388f687b'
[GameDB] No CRC-specific patch or default patch found
isoFile open ok: /home/doonoo/Games/Roms/PS2/Yakuza (USA).iso
	Image type  = DVD
 * CDVD Disk Open: DVD, Single layer or unknown:
 * * Track 1: Data (Mode 1) (2144000 sectors)
Opening plugins...
	Opening GS
EGL: Supported extensions: EGL_EXT_device_base EGL_EXT_device_enumeration EGL_EXT_device_query EGL_EXT_platform_base EGL_KHR_client_get_all_proc_addresses EGL_EXT_client_extensions EGL_KHR_debug EGL_EXT_platform_device EGL_EXT_platform_wayland EGL_KHR_platform_wayland EGL_EXT_platform_x11 EGL_KHR_platform_x11 EGL_MESA_platform_gbm EGL_KHR_platform_gbm EGL_MESA_platform_surfaceless
EGL: select X11 platform
ATTENTION: default value of option mesa_glthread overridden by environment.
ATTENTION: option value of option mesa_glthread ignored.
mesa: for the --simplifycfg-sink-common option: may only occur zero or one times!
mesa: for the --global-isel-abort option: may only occur zero or one times!
mesa: for the --amdgpu-atomic-optimizations option: may only occur zero or one times!
mesa: for the --structurizecfg-skip-uniform-regions option: may only occur zero or one times!
Current Renderer: OpenGL
Available VRAM/RAM:3840MB for textures
GSdx Lookup CRC:388F687B
GSdx Lookup CRC:388F687B
McdSlot 0 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd001.ps2
McdSlot 1 [File]: /home/doonoo/.config/PCSX2/memcards/Mcd002.ps2
Plugins opened successfully.
Failed to GetNetAdapter()
48000 SampleRate: 
Request SDL audio driver: pulseaudio
PAD: controller (Wireless Controller) detected with rumble support, GUID:050000004c050000cc09000000810000
Opened SDL audio driver: pulseaudio
Closing plugins...
	Closing GS
Delete 0 Shaders, 53 Programs, 28 Pipelines
Plugins closed successfully.
Shutting down plugins...
Plugins shutdown successfully.
(GameDB) Unloading...
